# Profiles
- Work with different environments 
- Decouple the infrastructure from the code
- For every profile create a file under `src/main/resources` directory
- The file name should be `application-{profile}.properties` or `application-{profile}.yaml`
- Overriding principle of properties in the inceasing order
    - application.properties
    - application.yaml 
    - Environmental variables
    - Command line arguments
- Passing the command line arguments 
   - IDE as VM arguments
   - `java -jar spring-boot-demo-0.0.1-SNAPSHOT.jar --spring.profiles.active=qa --server.port=8555`    

## Profile Activation
 - application.yaml 
    `spring.profiles.active = @activatedProperties@`
 - In the `pom.xml` file
 ```xml
  <profiles>
        <profile>
            <id>dev</id>
            <properties>
                <activatedProperties>dev</activatedProperties>
            </properties>
        </profile>
        <profile>
            <id>qa</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <activatedProperties>qa</activatedProperties>
            </properties>
        </profile>
    </profiles>
 ```   
- While building the project using maven use the below commands
```
 mvn clean package -P <profile-id>
 ./mvnw.cmd spring-boot:run -P <profile-id>
```

## Spring Data JPA - One to Many relatioship

- One to One
- One to Many
- Many to Many 

- One to Many
    - On the many side 
    ```java
        @ManyToOne(fetch = EAGER)
        @JoinColumn(name="order_id")
        @OnDelete(action = CASCADE)
        @JsonIgnore
        @Setter
        private Order order;
    ```   
   - On the one side
   ```java
       @OneToMany(mappedBy = "order", cascade = ALL)
       private Set<OrderLineItem> lineItems = new HashSet<>();
   ```
   - Add a convininece scaffolding code on the One side
   ```java
   //scaffolding method to set the bidirectional mapping
    public void addOrderLineItem(OrderLineItem orderLineItem){
        orderLineItem.setOrder(this);
        this.lineItems.add(orderLineItem);
    }
   ```      
   - Testing using Postman client
   ```json
   {
        "totalPrice": 50000,
        "orderDate": "2020-06-16",
        "lineItems": [
            {
                "qty": 1,
                "price": 55000
            }
        ]
    }
  
   [
    {
        "orderId": 1,
        "totalPrice": 50000,
        "orderDate": "2020-06-16",
        "lineItems": [
            {
                "id": 2,
                "qty": 1,
                "price": 55000
            }
        ]
    }
]
```
   
## Validation 
- Spring Boot provides starters for validation
```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>
```
  - In the `Order` entity add the annotations 
```java
   @Max(value = 50000, message = "Order price should be more than 25k and less than 50K")
    @Min(value = 25000, message = "Order price should be more than 25k and less than 50K")
    private double totalPrice;

    @NotNull(message = "Order date cannot be null")
    private LocalDate orderDate;
```
  - In the `Controller` add the `@Valid` annotation
 
 ## Exception 
 - Create a class and add the `ControllerAdvice` annotation
 - Handle individual exceptions using `ExceptionHandler` annotation
 - Specify the `Exception` class to handle the `Exception` 

