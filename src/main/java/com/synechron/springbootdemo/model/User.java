package com.synechron.springbootdemo.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "users")
@Setter
@Getter@EqualsAndHashCode(of = "id")
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long id;

    @Column(name="email_address", nullable = false, unique = true)
    private String emailAddress;

    @Column(name="password", nullable = false)
    private String password;

    @ManyToMany(mappedBy = "users", cascade = CascadeType.PERSIST, fetch = EAGER)
    private Set<Role> roles;
}