package com.synechron.springbootdemo.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class DomainPasswordEncoder {

    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        String encryptedPassword1 = encoder.encode("welcome");
        String encryptedPassword2 = encoder.encode("welcome");
        String encryptedPassword3 = encoder.encode("welcome");

        System.out.println(encryptedPassword1);
        System.out.println(encryptedPassword2);
        System.out.println(encryptedPassword3);

        System.out.println(encoder.matches("pradeep", encryptedPassword1));
        System.out.println(encoder.matches("pradeep", encryptedPassword2));
        System.out.println(encoder.matches("pradeep", encryptedPassword3));
    }
}