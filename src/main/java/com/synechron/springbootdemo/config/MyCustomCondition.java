package com.synechron.springbootdemo.config;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

@AllArgsConstructor
public class MyCustomCondition implements Condition {

    private Environment environment;

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String[] activeProfiles = environment.getActiveProfiles();
        for(String profile: activeProfiles){
            if (profile.equalsIgnoreCase("dev")){
                return true;
            }
        }
        return false;
    }
}