package com.synechron.springbootdemo.config;

import com.synechron.springbootdemo.model.DomainUserDetails;
import com.synechron.springbootdemo.service.DomainUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import sun.security.util.Password;

import static org.springframework.http.HttpMethod.*;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DomainUserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
       /*         .inMemoryAuthentication()
                .withUser("kiran")
                .password(passwordEncoder().encode("welcome"))
                .roles("USER")
                .and()
                .withUser("vinay")
                .password(passwordEncoder().encode("welcome"))
                .roles("USER", "ADMIN");
        */
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable();
        http
                .authorizeRequests()
                .antMatchers(GET, "/api/v1/orders/**")
                    .hasAnyRole("USER", "ADMIN")
                .antMatchers(POST, "/api/v1/orders/**")
                    .hasRole("ADMIN")
                .antMatchers(DELETE, "/api/v1/orders/**")
                    .hasRole("ADMIN")
                .antMatchers("/login", "/logout")
                    .permitAll()
                .anyRequest()
                    .fullyAuthenticated()
                .and()
                    .httpBasic()
                .and()
                    .formLogin();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}