package com.synechron.springbootdemo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.cloud.CloudPlatform;
import org.springframework.boot.system.JavaVersion;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Logger;

@Configuration
@Slf4j
public class AppConfig {
    //you do not have to declare variable when you are using @Slf4J annotation which is coming from Lombok
    private static final Logger logger = Logger.getLogger("AppConfig");

    @Bean
    @ConditionalOnProperty(name = "app.profile.value", havingValue = "dev", matchIfMissing = true)
    public Customer customer(){
        return new Customer();
    }

    @Bean
    @ConditionalOnMissingBean(name="customer")
    public Payment payment(){
        return new Payment();
    }

    @Bean
    @ConditionalOnJava(JavaVersion.EIGHT)
    public MyCustomDataSource customDataSource(){
        return new MyCustomDataSource();
    }

    @Bean
    @ConditionalOnMissingClass("sdfsdfsdfsdfsdfsdfsdf")
    public MissingClass missingClass(){
        return new MissingClass();
    }

    @Bean
    @ConditionalOnCloudPlatform(CloudPlatform.CLOUD_FOUNDRY)
    public AWSLoadBalancer loadBalancer(){
        return new AWSLoadBalancer();
    }

    @Bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate =  new RestTemplate();
        restTemplate.getInterceptors().add(new CustomHttpClientInterceptor());
        return  restTemplate;
    }

}

@Slf4j
class CustomHttpClientInterceptor implements ClientHttpRequestInterceptor{

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        log.info("Interceptor in the rest template");
        request.getHeaders().add("Authorization", UUID.randomUUID().toString());
        log.info("-------------------Headers-------------------------");
        log.info(request.getHeaders().get("Authorization").toString());
        log.info("-------------------Headers-------------------------");
        return execution.execute(request, body);
    }
}

class AWSLoadBalancer{

}

class GCPLoadBalancer {

}

class MissingClass {

}

class Customer {

}

class Payment {

}

class MyCustomDataSource{

}