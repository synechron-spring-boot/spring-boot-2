package com.synechron.springbootdemo.service;

import com.synechron.springbootdemo.dao.UserRepository;
import com.synechron.springbootdemo.model.DomainUserDetails;
import com.synechron.springbootdemo.model.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class DomainUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String emailAddress) throws UsernameNotFoundException {
        User user = this.userRepository.findByEmailAddress(emailAddress).orElseThrow(() -> new UsernameNotFoundException("Invalid User"));
        DomainUserDetails userDetails = new DomainUserDetails(user);
        log.info("User Details ---> " , userDetails);
        return userDetails;
    }
}