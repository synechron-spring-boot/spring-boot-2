package com.synechron.springbootdemo.service;

import com.synechron.springbootdemo.dao.OrderDAO;
import com.synechron.springbootdemo.dao.OrderRepository;
import com.synechron.springbootdemo.model.Order;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderDAO;

    private RestTemplate restTemplate;

    @Override
    public Order saveOrder(Order order) {
        return this.orderDAO.save(order);
    }

    @Override
    public Set<Order> fetchOrders() {
        return new HashSet<>(this.orderDAO.findAll());
    }


    @Override
    public Order fetchOrderByOrderId(long orderId) {
        return this.orderDAO.findById(orderId)
                .orElseThrow(OrderServiceImpl::invalidOrderId);
    }

    private static IllegalArgumentException invalidOrderId() {
        return new IllegalArgumentException("Invalid Order Id");
    }

    @Override
    public void archiveOrderByOrderId(long orderId) {
        this.orderDAO.deleteById(orderId);
    }

    @Override
    public String fetchAllCourses() {
        return this.restTemplate.getForEntity("https://my-json-server.typicode.com/prashdeep/courseflix/courses/",String.class).getBody();

    }
}